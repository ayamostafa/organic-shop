import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import { Observable } from 'rxjs/Observable';
import {ShoppingCart} from 'shared/models/shopping-cart';
//import {Order} from './../models/order';
import {ShopingCartService} from 'shared/services/shoping-cart.service';
//import {OrderService} from './../order.service';
//import {AuthService} from './../auth.service';
//import {Router} from '@angular/router';
@Component({
    selector: 'app-check-out',
    templateUrl: './check-out.component.html',
    styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {
   // shipping = {};
     cart$: Observable<ShoppingCart>;
    cartSubscription: Subscription;
   // userSubscription: Subscription;
    //userId:string;
    constructor(private shoppingCartService: ShopingCartService,
    //    private orderService: OrderService,
      //  private authService: AuthService,
        //private router:Router
        
    ) {

    }

      async ngOnInit() { 
    this.cart$ = await this.shoppingCartService.getCart();
  }
//        async ngOnInit() {
//        let cart$ = await this.shoppingCartService.getCart();
//        this.cartSubscription = cart$.subscribe(cart => this.cart = cart);
////        this.userSubscription = this.authService.user$.subscribe(
////            user => this.userId = user.uid
////        );
//    }
//   async placeOrder() {
//        console.log(this.shipping);
//        let order = new Order(this.userId, this.shipping,this.cart);
//       let result = await this.orderService.placeOrder(order);
//        this.router.navigate(['/order-success',result.key]);
//    }
//    ngOnDestroy() {
//        this.cartSubscription.unsubscribe();
//      //  this.userSubscription.unsubscribe();
//    }

}
