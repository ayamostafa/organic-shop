import { Component, OnInit } from '@angular/core';
import {ShopingCartService} from 'shared/services/shoping-cart.service';
import {Observable} from 'rxjs';
import {ShoppingCart} from 'shared/models/shopping-cart';
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
    cart$;
  constructor(private shoppingCartService: ShopingCartService) { }

  async ngOnInit() {
      this.cart$ = await this.shoppingCartService.getCart();
     //console.log(this.cart$.items);
   }
   async clearCart(){
       this.shoppingCartService.clearCart();
   }
     

}
