import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProductService} from 'shared/services/product.service';
import {ShopingCartService} from 'shared/services/shoping-cart.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {Product} from 'shared/models/product';
import {ShoppingCart} from 'shared/models/shopping-cart';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    products: Product[] = [];
    filteredProducts: Product[] = [];

    category: string;
    cart$: Observable<ShoppingCart>;
    subscribtion: Subscription;
    constructor(
        private route: ActivatedRoute,
        private productService: ProductService,
        private shoppingService: ShopingCartService
    ) {
    }

    async ngOnInit() {
        this.cart$ = await this.shoppingService.getCart();
        this.populateProducts();

        //     this.subscribtion= (await this.shoppingService.getCart()).subscribe(cart=>
        //       this.cart =cart
        //       );
        this.cart$ = await this.shoppingService.getCart();
    }
    //     
    //    ngOnDestroy(){
    //        this.subscribtion.unsubscribe();
    //    }
    applyFilter() {
        this.filteredProducts = (this.category) ?
            this.products.filter(p => p.category === this.category) :
            this.products;
    }
    populateProducts() {
        this.productService.getAll()
            .switchMap(p => {
                this.products = p;
                return this.route.queryParamMap;
            }).subscribe(params => {

                this.category = params.get('category');
                this.applyFilter();


            });
    }
}
