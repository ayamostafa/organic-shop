import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, FirebaseObjectObservable} from 'angularfire2/database';
import {Product} from 'shared/models/product';
import {ShoppingCart} from 'shared/models/shopping-cart';

@Injectable()
export class ShopingCartService {

    constructor(private db: AngularFireDatabase) {}
    create() {
        return this.db.list('/shopping-carts').push({
            dateCreated: new Date().getTime()
        })
    }
    getItem(cartId, p: Product) {
        return this.db.object('/shopping-carts/' + cartId + '/items/' + p.$key)

    }
    async getCart(): Promise<Observable<ShoppingCart>> {
        let cartId = await this.getOrCreateCartID();
        return this.db.object('/shopping-carts/' + cartId)
            .map(x => new ShoppingCart(x.items));
    }
    private async getOrCreateCartID(): Promise<string> {
        let cartId = localStorage.getItem('cart');
        if (cartId) return cartId;
        let result = await this.create();
        localStorage.setItem('cart', result.key);
        return result.key;
    }
    async  addToCart(p: Product) {
        this.updateItem(p, 1);
    }
    getQuantity() {
        let cartId = this.getOrCreateCartID();
        return 1;
    }
    async removeFromCart(p: Product) {
        this.updateItem(p, -1);
    }
    private async  updateItem(product: Product, change: number) {
        let cartId = await this.getOrCreateCartID();
        let item$ = this.getItem(cartId, product);
        item$.take(1).subscribe(item => {
            let quantity = (item.quantity || 0) + change;
            if(quantity===0) item$.remove();
            else
            item$.update({
                //product: p,
                title: product.title,
                imageUrl: product.imageUrl,
                price: product.price,
                quantity: ((item.quantity || 0) + change)

            });
        });
    }
    async clearCart(){
        let cartId = await this.getOrCreateCartID();
        this.db.object('/shopping-carts/' + cartId+'/items').remove();
    }
}
