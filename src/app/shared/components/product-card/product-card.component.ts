import { Component, OnInit,Input } from '@angular/core';
import {ShopingCartService} from 'shared/services/shoping-cart.service';
import {Product} from 'shared/models/product';
import {ShoppingCart} from 'shared/models/shopping-cart';
@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent  {
    

    @Input('product') product: Product;
  @Input('show-actions') showActions = true;
  @Input('shopping-cart') shoppingCart: ShoppingCart; 

  constructor(private cartService:ShopingCartService) { }

     addToCart(product:Product) {
        this.cartService.addToCart(product);
    }
}
