import {Component, OnInit, Input} from '@angular/core';
import {ShopingCartService} from 'shared/services/shoping-cart.service';

@Component({
    selector: 'product-quantity',
    templateUrl: './product-quantity.component.html',
    styleUrls: ['./product-quantity.component.css']
})
export class ProductQuantityComponent implements OnInit {
    @Input('product') product;
    @Input('shopping-cart') shoppingCart;
    constructor(private cartService: ShopingCartService) {}

    ngOnInit() {
    }
 
    removeFromCart() {
        this.cartService.removeFromCart(this.product);
    }
  addToCart(){
      this.cartService.addToCart(this.product);
  }
}