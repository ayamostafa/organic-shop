/* 
 * Created by Aya Mostafa.
 * Date: Oct 13, 2018
 */

import {ShoppingCartItem} from './shopping-cart-item';
import {Product} from './product';
export class ShoppingCart {
    public items: ShoppingCartItem[] = [];
    //constructor(public   items:ShoppingCartItem[]){
    constructor(private itemsMap: {[productId: string]: ShoppingCartItem}) {
        this.itemsMap = itemsMap || {};
        for (let productId in itemsMap) {
            let item = itemsMap[productId];
            //this.items.push(itemsMap[productId]);
            //    this.items.push(new ShoppingCartItem(item.product,item.quantity));
            //           let x = new ShoppingCartItem();
            //           Object.assign(x,item);
            //           x.$key = productId;
            //           this.items.push(x);
            //           let x = new ShoppingCartItem({
            //               ...item,
            //               $key: productId
            //           });
            this.items.push(new ShoppingCartItem({
                ...item,
                $key: productId
            }));
        }
    }

    get totalItemsCount() {
        let count = 0;
        for (let productId in this.items) {
            count += this.items[productId].quantity;
            //    console.log(count)
        }
        return count;
    }
    get productIds() {
        return Object.keys(this.items);
    }
    get totalPrice() {
        let sum = 0;
        for (let productId in this.items) {
            sum += this.items[productId].totalPrice;
        }
        return sum;
    }
    public getQuantity(product: Product) {
        let item = this.itemsMap[product.$key];
        return item ? item.quantity : 0;
        //        ///  console.log(this.shoppingCart);
        //        if (!this.shoppingCart) return 0;
        //        let item = this.shoppingCart.items[this.product.$key];
        //        return item ? item.quantity : 0;

    }
}
