/* 
 * Created by Aya Mostafa.
 * Date: Oct 20, 2018
 */

import {ShoppingCart} from './../models/shopping-cart';
export class Order{
    items:any[];
    datePlaced:number;
    constructor(
        public userId:string,
        public shipping:any,
        shoppingCart: ShoppingCart
        ){
        this.datePlaced = new Date().getTime();
              this.items = shoppingCart.items.map(
                i => {
                    return {
                        product: {

                        },
                        quantity: i.quantity,
                        totalPrice: i.totalPrice,
                    }
                })
        };
    
}