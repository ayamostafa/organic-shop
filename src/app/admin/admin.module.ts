import { NgModule } from '@angular/core';
import { AdminOrdersComponent } from './components/admin-orders/admin-orders.component';
import { AdminProductFormComponent } from './components/admin-product-form/admin-product-form.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { SharedModule } from 'shared/shared.module';
import { AdminAuthGuard } from './services/admin-auth-guard.service';
import { AuthGuard } from 'shared/services/auth-guard.service';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      { 
        path: 'admin/product/new', 
        component: AdminProductFormComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
       { 
        path: 'admin/product/:id', 
        component: AdminProductFormComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
        { 
        path: 'admin/products', 
        component: AdminProductsComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
      { 
        path: 'admin/orders', 
        component: AdminOrdersComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      }
    ])    
  ],
  declarations: [
    AdminProductFormComponent,
    AdminProductsComponent,
    AdminOrdersComponent,
    
  ],
  providers: [
    AdminAuthGuard
  ]
})
export class AdminModule { }
