import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'shared/services/category.service';
import {ProductService} from 'shared/services/product.service';
import {Router,ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/take';
@Component({
  selector: 'app-admin-product-form',
  templateUrl: './admin-product-form.component.html',
  styleUrls: ['./admin-product-form.component.css']
})
export class AdminProductFormComponent implements OnInit {
  categories$;
  product={};
  id;
  constructor(private categoryService:CategoryService,
  private activatedRoute:ActivatedRoute,private router:Router,
      private productService:ProductService
      ) {
       this.id = this.activatedRoute.snapshot.paramMap.get('id');
      if(this.id) 
          this.productService.get(this.id).take(1).subscribe(p=> this.product=p);
   }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
  }
  saveProduct(form){
      console.log("clicked")
      if(form.valid){
      console.log(form);
      console.log(this.product);
          if (this.id) this.productService.update(this.id, this.product);
          else this.productService.create(form.value);
          window.setTimeout(e=>{
              this.router.navigate(['/admin/products']);
          },1000);
          
      }
  }
  delete(){
      if(!confirm('Are you sure you want to delete this item?')) return;
      this.productService.delete(this.id);
           window.setTimeout(e=>{
              this.router.navigate(['/admin/products']);
          },1000);
  }
}
