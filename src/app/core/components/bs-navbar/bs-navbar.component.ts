import {Observable} from 'rxjs';
import { AuthService } from 'shared/services/auth.service';
import { ShopingCartService } from 'shared/services/shoping-cart.service';
import { Component, OnInit } from '@angular/core';
import { AppUser } from 'shared/models/app-user';
import {ShoppingCart} from 'shared/models/shopping-cart';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit{
  appUser: AppUser;
  //shoppingCartItem:ShoppingCartItem;
  shoppingCartItemCount;
  cart$:Observable<ShoppingCart>;
  constructor(private auth: AuthService,
      private shoppinCartService:ShopingCartService) { 
    auth.appUser$.subscribe(appUser => this.appUser = appUser);
  }

  logout() {
    this.auth.logout();
  }
 async ngOnInit(){
      this.cart$=await this.shoppinCartService.getCart();
      //console.log( this.cart$);
      
  }

}
